import '../scss/main.scss'

document.addEventListener('DOMContentLoaded', () => {
    const wrapperContext = document.getElementById('game-wrapper')
    const doodler = document.getElementById('doodle');
    let countPosition = 0;
    let doodleBottomPosition = 0;
    let leaveWrapper = false;
    const platformCount = 5;
    let jumpBottomDoodle;
    let jumpTopDoodle;
    document.addEventListener('keydown', (e) => {
        leaveWrapper = doodler.offsetLeft > wrapperContext.offsetWidth - (doodler.offsetWidth + 40)
        if(e.keyCode == 39 && !leaveWrapper) { // --> кнопка вправо
            countPosition += 40;
            doodler.style.left = countPosition + 'px'
        } else if (e.keyCode == 37 && countPosition > 0) { // --> кнопка влево
            countPosition -= 40;
            doodler.style.left = countPosition + 'px'
        }
    })

    function createPlatform() {
        for(let i = 0; i < platformCount; i++) {
            let platformSpace = 1000 / platformCount
            let newPlatformBottom = 100 + i * platformSpace;
            let left = Math.random() * 400;
            const visual = document.createElement('div')
            visual.classList.add('platform')
            visual.style.left = left + "px"
            visual.style.bottom = newPlatformBottom + "px"

            wrapperContext.appendChild(visual)
        }
    }

    function movePlatform() {
        for(let i of document.querySelectorAll('.platform')) {
            let bottom = i.style.bottom.slice(0,-2) - 10;
            i.style.bottom = bottom + 'px'
        }
    }

    function jump() {
        jumpBottomDoodle = setInterval(() => {
            if(doodleBottomPosition < 400) {
                doodleBottomPosition += 30;
                doodler.style.bottom = doodleBottomPosition + 'px'
            } else {
                fall()
            } 
        }, 100)
    }

    function fall() {
        clearInterval(jumpBottomDoodle)
        jumpTopDoodle = setInterval(() => {
            doodleBottomPosition -= 30;
            doodler.style.bottom = doodleBottomPosition + 'px'
        }, 100)
    }

    createPlatform()
    // jump()
    // setInterval(() => {
    //     movePlatform()
    // }, 100)
})